package org.liurongchan.model;


/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-9-27 下午2:24:47
 * 
 *          magazine
 */
public class Magazine implements Comparable<Magazine>{

	private int id;
	private String name;
	private String desc;
	private String thumbnail;
	private String thumbnail_medium;
	private String thumbnail_thumb;
	private String picture;
	private String category;


	public Magazine(int id, String name, String desc, String thumbnail,
			String thumbnail_medium, String thumbnail_thumb, String picture, String category) {
		super();
		this.id = id;
		this.name = name;
		this.desc = desc;
		this.thumbnail = thumbnail;
		this.thumbnail_medium = thumbnail_medium;
		this.thumbnail_thumb = thumbnail_thumb;
		this.picture = picture;
		this.category = category;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}

	public String getThumbnail_medium() {
		return thumbnail_medium;
	}

	public void setThumbnail_medium(String thumbnail_medium) {
		this.thumbnail_medium = thumbnail_medium;
	}

	public String getThumbnail_thumb() {
		return thumbnail_thumb;
	}

	public void setThumbnail_thumb(String thumbnail_thumb) {
		this.thumbnail_thumb = thumbnail_thumb;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	@Override
	public int compareTo(Magazine another) {
		return this.id - another.id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}
}
