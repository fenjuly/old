package org.liurongchan.model;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-10-21 下午1:12:02 评论类
 */
public class Comment {
	private int id;

	public Comment(int id, String commenter, String content, int article_id) {
		super();
		this.id = id;
		this.commenter = commenter;
		this.content = content;
		this.article_id = article_id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCommenter() {
		return commenter;
	}

	public void setCommenter(String commenter) {
		this.commenter = commenter;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getArticle_id() {
		return article_id;
	}

	public void setArticle_id(int article_id) {
		this.article_id = article_id;
	}

	private String commenter;
	private String content;
	private int article_id;
}
