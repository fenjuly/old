package org.liurongchan.model;

import java.io.Serializable;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-10-21 下午1:14:34 头条类
 */
public class HeadTitle implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -7112567554960979999L;
	private int id;
	private String image;
	private String picture;
	private String type;// 1.url 2.magazine 3.article
	private String value;

	public HeadTitle(int id, String image, String picture, String type,
			String value) {
		super();
		this.id = id;
		this.image = image;
		this.picture = picture;
		this.type = type;
		this.value = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
