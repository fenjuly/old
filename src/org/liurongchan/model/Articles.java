package org.liurongchan.model;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-9-27 下午2:30:57
 * 
 *          articles
 */
public class Articles implements Comparable<Articles>{

	private int id;
	private String title;
	private String summary;
	private String author;
	private String content;
	private String head_image;
	private String head_image_medium;
	private String head_image_thumb;
	private int magazine;
	private String picture;
	private String category;
	private String head_picture;

	public Articles(int id, String title, String summary, String author,
			String content, String head_image, String head_image_medium,
			String head_image_thumb, int magazine, String picture, String category, String head_picture) {
		super();
		this.id = id;
		this.title = title;
		this.summary = summary;
		this.author = author;
		this.content = content;
		this.head_image = head_image;
		this.head_image_medium = head_image_medium;
		this.head_image_thumb = head_image_thumb;
		this.magazine = magazine;
		this.picture = picture;
		this.setCategory(category);
		this.setHead_picture(head_picture); 
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getHead_image() {
		return head_image;
	}

	public void setHead_image(String head_image) {
		this.head_image = head_image;
	}

	public String getHead_image_medium() {
		return head_image_medium;
	}

	public void setHead_image_medium(String head_image_medium) {
		this.head_image_medium = head_image_medium;
	}

	public String getHead_image_thumb() {
		return head_image_thumb;
	}

	public void setHead_image_thumb(String head_image_thumb) {
		this.head_image_thumb = head_image_thumb;
	}

	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}

	public int getMagazine() {
		return magazine;
	}

	public void setMagazine(int magazine) {
		this.magazine = magazine;
	}

	@Override
	public int compareTo(Articles another) {
		return this.id - another.id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getHead_picture() {
		return head_picture;
	}

	public void setHead_picture(String head_picture) {
		this.head_picture = head_picture;
	}

}
