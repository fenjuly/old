package org.liurongchan.young;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.liurongchan.db.DBManager;
import org.liurongchan.json.GetComment;
import org.liurongchan.model.Articles;
import org.liurongchan.model.Comment;
import org.liurongchan.utils.Utils;

import com.flurry.android.FlurryAgent;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.text.InputFilter;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.text.style.TypefaceSpan;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AbsListView;
import android.widget.AbsListView.OnScrollListener;
import android.widget.EditText;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-9 上午12:21:48 类说明
 */
public class Content_Activity extends ActionBarActivity implements
		OnScrollListener, OnClickListener {

	private Context mContext;

	private ImageView mArticleimg;
	private TextView mTitle;
	private TextView mAuthor;
	private TextView mContent;

	private EditText mEditText;

	private int mAticle_id;

	private DBManager dbManager;

	private Articles article;

	private ArrayList<Comment> comments;

	private LinearLayout mComments;
	private LayoutInflater mLayoutInflater;
	private View mLoadMoreComment;
	private Button mLoadMoreButton;
	private int mCommentCount;
	private boolean mCommentFinished;
	private int isFirst = 1;
	private int mStep = 5;

	private LinearLayout[] linearlayout;
	private SpannableString msp;
	
	private Bitmap bmp;
	

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mContext = this;
		dbManager = new DBManager(mContext);
		mAticle_id = getIntent().getExtras().getInt("ArticleID");
		Articles a = dbManager.queryArticleById(mAticle_id);
		getSupportActionBar().setTitle(a.getTitle());
		getSupportActionBar().setLogo(R.drawable.logo_color);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setBackgroundDrawable(
				this.getBaseContext().getResources()
						.getDrawable(R.drawable.actionbar_background));
		setContentView(R.layout.article_content);
		mLayoutInflater = (LayoutInflater) mContext
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		mArticleimg = (ImageView) this.findViewById(R.id.article_image);
		mTitle = (TextView) this.findViewById(R.id.title);
		mAuthor = (TextView) this.findViewById(R.id.author);
		mContent = (TextView) this.findViewById(R.id.content);
		mComments = (LinearLayout) findViewById(R.id.comments);
		mEditText = (EditText) this.findViewById(R.id.comment_edit_text);
		mEditText.setOnClickListener(this);
 

		article = dbManager.queryArticleById(mAticle_id);

		if (article.getContent().equals("")) {
			new Refresh().execute("");
		}

		if (article.getPicture().equals("")) {
//			Picasso.with(mContext).load(article.getHead_image()).placeholder(R.drawable.placeholder_thumb)
//			.error(R.drawable.placeholder_fail)
//					.into(new Target() {
//						@SuppressWarnings("deprecation")
//						@Override
//						public void onBitmapLoaded(Bitmap bitmap,
//								LoadedFrom loadedFrom) {
//
//							// whatever algorithm here to compute size
//							float ratio = (float) bitmap.getHeight()
//									/ (float) bitmap.getWidth();
//							float heightFloat = ((float) (getWindowManager().getDefaultDisplay().getWidth()-56))
//									* ratio;
//
//							final android.view.ViewGroup.MarginLayoutParams layoutParams = (MarginLayoutParams) mArticleimg
//									.getLayoutParams();
//
//							layoutParams.height = (int) heightFloat;
//							layoutParams.width = (int) (getWindowManager().getDefaultDisplay().getWidth()-56);
//							mArticleimg.setLayoutParams(layoutParams);
//							mArticleimg.setImageBitmap(bitmap);
//							mArticleimg.invalidate();
//						}
//
//						@Override
//						public void onBitmapFailed() {
//						}
//
//					});
			GetBitmap get = new GetBitmap();
			get.execute(article.getHead_image());

		} else {
//			Picasso.with(mContext).load(new File(article.getPicture())).placeholder(R.drawable.placeholder_thumb)
//			.error(R.drawable.placeholder_fail)
//					.into(new Target() {
//						@SuppressWarnings("deprecation")
//						@Override
//						public void onBitmapLoaded(Bitmap bitmap,
//								LoadedFrom loadedFrom) {
//
//							// whatever algorithm here to compute size
//							float ratio = (float) bitmap.getHeight()
//									/ (float) bitmap.getWidth();
//							
//							float heightFloat = ((float) (getWindowManager().getDefaultDisplay().getWidth()-56))
//									* ratio;
//
//							final android.view.ViewGroup.MarginLayoutParams layoutParams = (MarginLayoutParams) mArticleimg
//									.getLayoutParams();
//
//							layoutParams.height = (int) heightFloat;
//							layoutParams.width = (int) (getWindowManager().getDefaultDisplay().getWidth()-56);
//							Bitmap bmp = Bitmap.createScaledBitmap(bitmap, (int)(getWindowManager().getDefaultDisplay().getWidth()-56), (int)heightFloat, true);
//							mArticleimg.setImageBitmap(bmp);
//							Thread r = new Rf();
//							r.start();
//						}
//
//						@Override
//						public void onBitmapFailed() {
//						}
//
//					});
			bmp =  BitmapFactory.decodeFile(article.getPicture());
			if(bmp == null){
				mArticleimg.setImageResource(R.drawable.big_bg);
			} else {
			float ratio = (float) bmp.getHeight()
					/ (float) bmp.getWidth();
			float widthFloat = (float) (getWindowManager().getDefaultDisplay().getWidth()-56);
			float heightFloat = ((float) (getWindowManager().getDefaultDisplay().getWidth()-56)) * ratio;
			bmp = Bitmap.createScaledBitmap(bmp, (int)widthFloat, (int)heightFloat, true);
			mArticleimg.setImageBitmap(bmp);
			}
		}
		
		
		Typeface tfTitle = Typeface.createFromAsset(getAssets(),
				"fonts/Roboto-Bold.ttf");
//		Typeface tf = Typeface.createFromAsset(getAssets(),
//				"fonts/Roboto-Thin.ttf");
		mTitle.setTypeface(tfTitle);
//		mAuthor.setTypeface(tf);
		msp = new SpannableString(article.getAuthor());
		msp.setSpan(new TypefaceSpan("monospace"), 0, article.getAuthor().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		msp.setSpan(new StyleSpan(android.graphics.Typeface.ITALIC), 0, article.getAuthor().length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);  //斜体
		mTitle.setText(article.getTitle());
		mAuthor.setText(msp);
		mContent.setText(article.getContent());

		new CommentsTask().execute();

	}
	
	
	
	class Rf extends Thread {

		@Override
		public void run() {
			mArticleimg.postInvalidate();
		}
		
	}
	
	class Refresh extends AsyncTask<String, String, String> {
		String content = "";

		@Override
		protected String doInBackground(String... arg0) {
			String strResult = connServerForResult("http://www.appao.com/youth/magazines/"
					+ article.getMagazine()
					+ "/articles/"
					+ mAticle_id
					+ ".json?type=text");
			try {
				JSONObject articleEntity = new JSONObject(strResult);
				content = articleEntity.getString("content");
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return content;
		}

		@Override
		protected void onPostExecute(String result) {
			mContent.setText(result);
			super.onPostExecute(result);
		}
	}

	/**
	 * 获取json数据
	 */
	private String connServerForResult(String strUrl) {
		// HttpGet对象

		HttpGet httpRequest = new HttpGet(strUrl);
		String strResul = "";
		try {
			// HttpClient对象
			HttpClient httpClient = new DefaultHttpClient();
			// 获得HttpResponse对象
			HttpResponse httpResponse = httpClient.execute(httpRequest);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// 取得返回的数据
				strResul = EntityUtils.toString(httpResponse.getEntity());
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return strResul;
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	public void comment() {

		final EditText editText = new EditText(mContext);
		editText.setHeight(mContext.getResources().getDimensionPixelSize(
				R.dimen.comment_edit_height));
		editText.setFilters(new InputFilter[] { new InputFilter.LengthFilter(
				250) });
		editText.setGravity(Gravity.LEFT | Gravity.TOP);
		AlertDialog.Builder commentDialog = new AlertDialog.Builder(mContext)
				.setTitle(R.string.publish_comment).setView(editText)
				.setNegativeButton(R.string.cancel_publish, null)
				.setPositiveButton(R.string.publish, null);
		final AlertDialog dialog = commentDialog.create();
		dialog.show();
		dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						final String content = editText.getText().toString();
						if (content.length() == 0) {
							Toast.makeText(mContext, R.string.comment_nothing,
									Toast.LENGTH_SHORT).show();
						} else if (content.length() < 5) {
							Toast.makeText(mContext,
									R.string.comment_too_short,
									Toast.LENGTH_SHORT).show();
						} else {
							new CommentThread(content).start();
							dialog.dismiss();
							Toast.makeText(mContext, "评论成功！",
									Toast.LENGTH_SHORT).show();
						}
					}
				});

	}

	private class CommentThread extends Thread {
		private String mContent;
		String urlPath = "http://www.appao.com/youth/magazines/"
				+ article.getMagazine() + "/articles/" + article.getId()
				+ "/comments";
		URL url;

		private CommentThread(String commentContent) {
			mContent = commentContent;
		}

		@Override
		public void run() {
			super.run();
			try {
				url = new URL(urlPath);
				JSONObject CommenterKey = new JSONObject();
				CommenterKey.put("content", mContent);
				CommenterKey.put("commenter", "匿名者");
				String content = String.valueOf(CommenterKey);
				HttpURLConnection conn = (HttpURLConnection) url
						.openConnection();
				conn.setConnectTimeout(5000);
				conn.setDoOutput(true);
				conn.setRequestMethod("POST");
				conn.setRequestProperty("Accept", "application/json");
				conn.setRequestProperty("Content-Type",
						"application/json; charset=utf-8");
				OutputStream os = conn.getOutputStream();
				os.write(content.getBytes());
				os.close();
				int code = conn.getResponseCode();
				if (code == 200 || code == 201) {
				}
			} catch (MalformedURLException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.comment_edit_text:
			comment();
			break;
		default:
			break;
		}

	}

	private class CommentsTask extends AsyncTask<Void, LinearLayout, Void> {

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (mLoadMoreComment != null) {
				mLoadMoreComment.findViewById(R.id.load_progressbar)
						.setVisibility(View.VISIBLE);
				mLoadMoreComment.findViewById(R.id.load_more_comment_btn)
						.setVisibility(View.GONE);
			}
		}

		@Override
		protected Void doInBackground(Void... params) {
			GetComment getComment = new GetComment(article.getMagazine(),
					mAticle_id);
			getComment.makeJsonToComment();
			comments = (ArrayList<Comment>) getComment.getComments();
			while (!getComment.isComplete()) {

			}

			if (comments.size() < mStep) {
				mCommentFinished = true;
			}
			ArrayList<LinearLayout> commentsLayout = new ArrayList<LinearLayout>();
			for (Comment comment : comments) {

				LinearLayout commentItem = (LinearLayout) mLayoutInflater
						.inflate(R.layout.comment_item, null);
				TextView content = (TextView) commentItem
						.findViewById(R.id.content);
				content.setText(comment.getContent());
				TextView username = (TextView) commentItem
						.findViewById(R.id.name);
				username.setText(comment.getCommenter());
				commentsLayout.add(commentItem);
			}
			mCommentCount += comments.size();
			linearlayout = commentsLayout.toArray(new LinearLayout[comments
					.size()]);
			publishProgress(commentsLayout.toArray(new LinearLayout[comments
					.size()]));
			return null;
		}

		@Override
		protected void onProgressUpdate(LinearLayout... values) {
			super.onProgressUpdate(values);
			if (mLoadMoreComment != null) {
				mComments.removeAllViews();
				mComments.addView(mEditText);
				mEditText.setOnClickListener((OnClickListener) mContext);
			}
			for (LinearLayout commentView : values) {
				mComments.addView(commentView);
			}
		}

		@Override
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			if (mCommentFinished == false) {
				mLoadMoreComment = mLayoutInflater.inflate(
						R.layout.comment_load_more, null);
				mLoadMoreButton = (Button) mLoadMoreComment
						.findViewById(R.id.load_more_comment_btn);
				if (isFirst == 1) {
					for (int i = 5; i < comments.size(); i++) {
						mComments.removeView(linearlayout[i]);
					}
					isFirst = 0;
				}
				mComments.addView(mLoadMoreComment);
				mLoadMoreButton.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View v) {
						new CommentsTask().execute();
					}
				});
			} else {
				if (mCommentCount > 5) {
					mLoadMoreButton.setText(R.string.no_more_comments);
					mLoadMoreButton.setEnabled(false);
				}
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.magazine_2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
		}
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, Utils.FLURRY_API_KEY);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
	
	public Bitmap getBitmap(String url) {

		Bitmap bitmap = null;
		try {
			HttpClient client = new DefaultHttpClient();
			URI uri = URI.create(url);
			HttpGet get = new HttpGet(uri);
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			long length = entity.getContentLength();
			Log.i("czb", " " + length);
			InputStream in = entity.getContent();
			if (in != null) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				bitmap = BitmapFactory.decodeStream(in);
				in.close();
				baos.close();
				return bitmap;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bitmap;

	}
	
	class GetBitmap extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			try {
				HttpClient client = new DefaultHttpClient();
				URI uri = URI.create(params[0]);
				HttpGet get = new HttpGet(uri);
				HttpResponse response = client.execute(get);
				HttpEntity entity = response.getEntity();
				long length = entity.getContentLength();
				Log.i("czb", " " + length);
				InputStream in = entity.getContent();
				if (in != null) {
					ByteArrayOutputStream baos = new ByteArrayOutputStream();

					bmp = BitmapFactory.decodeStream(in);
					in.close();
					baos.close();
				}
			} catch (ClientProtocolException e) {
				e.printStackTrace();
			} catch (IllegalStateException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return "";

		}
		
		@Override
		protected void onPostExecute(String result) {
			
			if(bmp == null) {
				mArticleimg.setImageResource(R.drawable.big_bg);
			}
			float ratio = (float) bmp.getHeight()
					/ (float) bmp.getWidth();
			@SuppressWarnings("deprecation")
			float widthFloat = (float) (getWindowManager().getDefaultDisplay().getWidth()-56);
			@SuppressWarnings("deprecation")
			float heightFloat = ((float) (getWindowManager().getDefaultDisplay().getWidth()-56)) * ratio;
			bmp = Bitmap.createScaledBitmap(bmp, (int)widthFloat, (int)heightFloat, true);
			mArticleimg.setImageBitmap(bmp);
			super.onPostExecute(result);
		}
		
	}
}
