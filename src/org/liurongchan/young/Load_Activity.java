package org.liurongchan.young;

import java.util.ArrayList;
import java.util.List;

import org.liurongchan.db.DBManager;
import org.liurongchan.json.GetHeadTitle;
import org.liurongchan.json.GetmagazineAndArticle;
import org.liurongchan.model.Articles;
import org.liurongchan.model.HeadTitle;
import org.liurongchan.model.Magazine;
import org.liurongchan.utils.Utils;

import com.flurry.android.FlurryAgent;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBarActivity;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-21 上午12:44:37 类说明
 */
public class Load_Activity extends ActionBarActivity {

	private Context mContext;

	private DBManager dbManager;

	private ArrayList<Magazine> magazines;

	private ArrayList<Articles> articles;

	private ArrayList<HeadTitle> headtitles;

	private final String SHARED_MAIN = "main";
	private final String IS_MFIRST = "ismfirst";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (getSupportActionBar() != null) {
			getSupportActionBar().hide();
		}
		mContext = this;
		dbManager = new DBManager(mContext);
		setContentView(R.layout.activity_load);
		final SharedPreferences mShared = mContext.getSharedPreferences(
				SHARED_MAIN, Context.MODE_PRIVATE);
		if (mShared.getInt(IS_MFIRST, 0) == 0) {
			Editor editor = mShared.edit();
			editor.putInt(IS_MFIRST, 1);
			editor.commit();
			MRefresh refresh = new MRefresh();
			refresh.execute("");
		} else {
			if(isNetworkConnected(mContext)) {
				HRefresh refresh = new HRefresh();
				refresh.execute("");
			}else {
			new Handler().postDelayed(new Runnable() {

				@Override
				public void run() {
					Intent intent = new Intent(mContext,
							Category_Activity.class);
					mContext.startActivity(intent);
					Load_Activity.this.finish();
				}
			}, 2000);
		}
			}
	}

	class MRefresh extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			GetmagazineAndArticle getmagazineAndArticle = new GetmagazineAndArticle(mContext);
			getmagazineAndArticle.GetMagazinesAndArticles();
			magazines = (ArrayList<Magazine>) getmagazineAndArticle
					.getMagazines();
			articles = (ArrayList<Articles>) getmagazineAndArticle
					.getArticles();
			while (!getmagazineAndArticle.isComplete()) {

			}

			GetHeadTitle getHeadTitle = new GetHeadTitle();
			getHeadTitle.makeJsonToHeadTitle();
			headtitles = (ArrayList<HeadTitle>) getHeadTitle.getHeadTitles();
			while (!getHeadTitle.isComplete()) {

			}
			dbManager.addMagazines(magazines);
			dbManager.addArticles(articles);
			dbManager.addHeadTitles(headtitles);
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			Intent intent = new Intent(mContext, Category_Activity.class);
			mContext.startActivity(intent);
			super.onPostExecute(result);
			Load_Activity.this.finish();
		}
	}

	class HRefresh extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			GetHeadTitle getHeadTitle = new GetHeadTitle();
			getHeadTitle.makeJsonToHeadTitle();
			headtitles = (ArrayList<HeadTitle>) getHeadTitle.getHeadTitles();
			while (!getHeadTitle.isComplete()) {

			}
			List<HeadTitle> hts = dbManager.queryHeadTitle();
			for (HeadTitle headTitle : hts) {
				dbManager.deleteHeadTitle(headTitle.getId());
			}
			dbManager.addHeadTitles(headtitles);
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			Intent intent = new Intent(mContext, Category_Activity.class);
			mContext.startActivity(intent);
			super.onPostExecute(result);
			Load_Activity.this.finish();
		}
	}
	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, Utils.FLURRY_API_KEY);
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
	
	public boolean isNetworkConnected(Context context) {
		if (context != null) {
			ConnectivityManager mConnectivityManager = (ConnectivityManager) context
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo mNetworkInfo = mConnectivityManager
					.getActiveNetworkInfo();
			if (mNetworkInfo != null) {
				return mNetworkInfo.isAvailable();
			}
		}
		return false;
	}
}
