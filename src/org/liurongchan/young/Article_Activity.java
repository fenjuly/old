package org.liurongchan.young;

import java.util.ArrayList;
import java.util.List;

import org.liurongchan.adapter.ArticleAdapter;
import org.liurongchan.db.DBManager;
import org.liurongchan.item.CategoryItem;
import org.liurongchan.item.ContentItem;
import org.liurongchan.item.ListItems;
import org.liurongchan.model.Articles;
import org.liurongchan.model.Magazine;
import org.liurongchan.utils.Utils;

import com.flurry.android.FlurryAgent;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-8 下午9:02:42 类说明
 */
public class Article_Activity extends ActionBarActivity {


	private Context mContext;
	private LayoutInflater mLayoutInflater;

	private int magazineId;

	private ListView articleListView;

	private ArrayList<Articles> dbarticles;
	private ArrayList<Articles> articles;

	private List<String> categories;

	private int[] positions;
	private ArticleAdapter articleAdapter;

	private DBManager dbManager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		magazineId = getIntent().getExtras().getInt("MagazineID");
		mContext = this;
		dbManager = new DBManager(mContext);
		Magazine mg = dbManager.queryMagazineById(magazineId);
		getSupportActionBar().setTitle(mg.getName());
		getSupportActionBar().setLogo(R.drawable.logo_color);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setBackgroundDrawable(this.getBaseContext().getResources().getDrawable(R.drawable.actionbar_background));
		mLayoutInflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		setContentView(R.layout.article_layout);
		mContext = this;
		initView();
		initValidata();
		bindData();
		initListener();
	}

	private void initView() {
		articleListView = (ListView) this.findViewById(R.id.articleList);
	}

	private void initValidata() {
		articles = new ArrayList<Articles>();

		dbarticles = dbManager.queryArticle();
		categories = new ArrayList<String>();

		for (int i = 0; i < dbarticles.size(); i++) {
			if (dbarticles.get(i).getMagazine() == magazineId) {
				articles.add(dbarticles.get(i));
			}
		}

		for (int j = 0; j < articles.size(); j++) {
			if (j == 0) {
				categories.add(articles.get(j).getCategory());
			} else {
				boolean findsameCategory = false;
				for (int i = 0; i < categories.size(); i++) {
					if (categories.get(i).equals(articles.get(j).getCategory())) {
						findsameCategory = true;
					}
				}
				if (findsameCategory == false) {
					categories.add(articles.get(j).getCategory());
				}
			}
		}
		List<ListItems> mListItems = new ArrayList<ListItems>();
		positions = new int[2 * articles.size()];
		int count = 0;
		for (int i = 0; i < categories.size(); i++) {
			CategoryItem label = new CategoryItem(categories.get(i), mContext);
			mListItems.add(label);
			positions[count++] = 0;
			for (int j = 0; j < articles.size(); j++) {
				if (articles.get(j).getCategory().equals(categories.get(i))) {
					ContentItem contentItem = new ContentItem(articles.get(j)
							.getHead_picture(), articles.get(j).getHead_image_thumb(),
							articles.get(j).getTitle(), articles.get(j)
									.getSummary(), articles.get(j).getId(), mContext);
					mListItems.add(contentItem);
					positions[count++] = articles.get(j).getId();
				}
			}
		}

		articleAdapter = new ArticleAdapter(mListItems,
				articleListView.getContext(),
				mLayoutInflater);
	}

	private void bindData() {
		articleListView.setAdapter(articleAdapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.magazine_2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right); 
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
		}
		return super.onKeyDown(keyCode, event);
	}
	
	private void initListener() {
		// articleListView.setOnItemClickListener(new OnItemClickListener() {
		//
		// @Override
		// public void onItemClick(AdapterView<?> parent, View view,
		// int position, long id) {
		// Intent intent = new Intent();
		// intent.setClass(getActivity(), Article_Activity.class);
		// Bundle bl = new Bundle();
		// bl.putInt("magazineId", magazineId);
		// bl.putInt("articleId", positions[position - 1]);
		// intent.putExtras(bl);
		// startActivity(intent);
		// getActivity().overridePendingTransition(
		// android.R.anim.slide_in_left,
		// android.R.anim.slide_out_right);
		// }
		// });
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, Utils.FLURRY_API_KEY);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
}
