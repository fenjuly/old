package org.liurongchan.young;

import java.util.ArrayList;

import org.liurongchan.adapter.CategoryAdapter;
import org.liurongchan.adapter.ShowGalleryPagerAdapter;
import org.liurongchan.db.DBManager;
import org.liurongchan.json.GetHeadTitle;
import org.liurongchan.json.GetmagazineAndArticle;
import org.liurongchan.model.Articles;
import org.liurongchan.model.HeadTitle;
import org.liurongchan.model.Magazine;
import org.liurongchan.utils.Utils;
import org.liurongchan.young.R.anim;
import org.liurongchan.young.R.drawable;

import com.flurry.android.FlurryAgent;
import com.viewpagerindicator.PageIndicator;
import com.viewpagerindicator.UnderlinePageIndicator;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.PointF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.AbsListView.OnScrollListener;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-11 下午11:01:20 类说明
 */
public class Category_Activity extends ActionBarActivity implements
		OnScrollListener {

	private ListView categoryListView;

	private CategoryAdapter mCategoryAdapter;
	private Context mContext;

	private ViewPager mShowPager;
	private PageIndicator mShowIndicator;
	private ShowGalleryPagerAdapter mShowAdapter;

	private LayoutInflater mLayoutInflater;
	private DBManager dbManager;

	private int Mwhich;

	private ArrayList<Magazine> dbmagazines;
	private ArrayList<Magazine> magazines;

	private ArrayList<Articles> dbarticles;
	private ArrayList<Articles> articles;

	private ArrayList<HeadTitle> dbheadtitles;
	private ArrayList<HeadTitle> headtitles;

	private boolean findSameMagazine = false;
	private boolean findSameArticle = false;

	private final String SHARED_MAIN = "main";
	private final String IS_MFIRST = "ismfirst";

	protected MenuItem refreshItem;

	private MenuItem refresh;
	private MenuItem download;

	private static int DOWNLOAD = 0;
	private static int REFRESH = 1;
	
	private int isdownloading = 0;

	// notification

	private int progress = 0;

	private static NotificationManager notificationManager;

	private boolean finished = false;
	private boolean isf = false;
	private Notification.Builder builder;
	

	private Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			if (!isf) {
				builder = new Notification.Builder(Category_Activity.this);
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_LAUNCHER);
				intent.setClass(mContext, Category_Activity.class);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK|Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
				PendingIntent contentIntent = PendingIntent.getActivity(mContext, 0, intent, 0);
				builder.setContentIntent(contentIntent);
				builder.setTicker("开始离线文章...");
				builder.setContentTitle("正在离线文章")
						.setContentText("0%")
						.setSmallIcon(R.drawable.down);
				builder.setProgress(100, 0, false);
				notificationManager.notify(0, builder.build());
				isf = true;
			}

			builder.setContentText(progress + "%").setProgress(100, progress, false);
			 notificationManager.notify(0, builder.build());
			super.handleMessage(msg);
		};
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getSupportActionBar().setTitle("年轻人");
		getSupportActionBar().setLogo(R.drawable.logo_color);
		getSupportActionBar().setBackgroundDrawable(
				this.getBaseContext().getResources()
						.getDrawable(R.drawable.actionbar_background));
		mContext = this;
		mLayoutInflater = (LayoutInflater) this
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		setContentView(R.layout.category_layout);
		notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		initView();
		initValidata();
		bindData();
	}

	private void initView() {
		categoryListView = (ListView) this.findViewById(R.id.categoryList);
		View headerView = mLayoutInflater.inflate(R.layout.gallery_item, null,
				false);
		categoryListView.addHeaderView(headerView);
		mShowPager = (ViewPager) headerView.findViewById(R.id.pager);
		mShowPager.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				PointF downP = new PointF();
				PointF curP = new PointF();
				int act = event.getAction();
				if (act == MotionEvent.ACTION_DOWN
						|| act == MotionEvent.ACTION_MOVE
						|| act == MotionEvent.ACTION_UP) {
					((ViewGroup) v).requestDisallowInterceptTouchEvent(true);
					if (downP.x == curP.x && downP.y == curP.y) {
						return false;
					}
				}
				return false;
			}
		});
		mShowIndicator = (UnderlinePageIndicator) headerView
				.findViewById(R.id.indicator);

	}

	private void initValidata() {
		dbManager = new DBManager(mContext);

		dbheadtitles = dbManager.queryHeadTitle();
		dbmagazines = dbManager.queryMagazine();

		mCategoryAdapter = new CategoryAdapter(mContext, dbmagazines);
		final SharedPreferences mShared = mContext.getSharedPreferences(
				SHARED_MAIN, Context.MODE_PRIVATE);
		if (mShared.getInt(IS_MFIRST, 0) == 0) {
			Editor editor = mShared.edit();
			editor.putInt(IS_MFIRST, 1);
			editor.commit();
			MRefresh refresh = new MRefresh();
			refresh.execute("");
		}
	}

	private void bindData() {
		categoryListView.setAdapter(mCategoryAdapter);
		dbheadtitles = dbManager.queryHeadTitle();

		mShowAdapter = new ShowGalleryPagerAdapter(getSupportFragmentManager(),
				dbheadtitles);
		mShowPager.setAdapter(mShowAdapter);
		mShowIndicator.setViewPager(mShowPager);

	}

	class Refresh extends AsyncTask<String, Integer, String> {

		@Override
		protected String doInBackground(String... params) {
			final SharedPreferences mShared = mContext.getSharedPreferences(
					SHARED_MAIN, Context.MODE_PRIVATE);
			GetmagazineAndArticle getmagazineAndArticle = new GetmagazineAndArticle(
					mContext);
			getmagazineAndArticle.initializeArcticleInstance();
			for (int i = 0; i < dbmagazines.size(); i++) {
				int id = dbmagazines.get(i).getId();
				getmagazineAndArticle.getSingleArticleContent(id);
				if (!finished) {
					int totalsize = mShared.getInt("count", 1);
					progress = getmagazineAndArticle.getA_count() * 100
							/ totalsize;
					publishProgress(progress);
					Message msg = new Message();
					handler.handleMessage(msg);
				}
				while (!getmagazineAndArticle.isComplete()) {
				}
			}
			articles = (ArrayList<Articles>) getmagazineAndArticle
					.getArticles();
			for (int i = 0; i < articles.size(); i++) {
				dbManager.updateArticle(articles.get(i));
			}
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			hideRefreshAnimation();
			refresh.setEnabled(true);
			final SharedPreferences mShared = mContext.getSharedPreferences(
					SHARED_MAIN, Context.MODE_PRIVATE);
				Editor editor = mShared.edit();
				editor.putInt("downloading", 0);
				editor.commit();
			Toast.makeText(mContext, "离线文章完成!", Toast.LENGTH_SHORT).show();
			isdownloading = 0;
			super.onPostExecute(result);
		}

		@Override
		protected void onProgressUpdate(Integer... values) {
			if (values[0] == 100) {
				finished = true;
				notificationManager.cancel(0);
			}
			super.onProgressUpdate(values);
		}

		@Override
		protected void onPreExecute() {
			handler.handleMessage(new Message());
			super.onPreExecute();
		}
	}

	class MRefresh extends AsyncTask<String, String, String> {

		@Override
		protected String doInBackground(String... params) {
			GetmagazineAndArticle getmagazineAndArticle = new GetmagazineAndArticle(
					mContext);
			getmagazineAndArticle.GetMagazinesAndArticles();
			magazines = (ArrayList<Magazine>) getmagazineAndArticle
					.getMagazines();
			articles = (ArrayList<Articles>) getmagazineAndArticle
					.getArticles();
			dbarticles = dbManager.queryArticle();
			while (!getmagazineAndArticle.isComplete()) {

			}

			if (articles.size() == 0) {
				for (int i = 0; i < dbarticles.size(); i++) {
					dbManager.deleteArticle(dbarticles.get(i));
				}
			} else {
				if (dbarticles.size() == 0) {
					dbManager.addArticles(articles);
				} else {
					for (int i = 0; i < articles.size(); i++) {
						for (int j = 0; j < dbarticles.size(); j++) {
							findSameArticle = articles.get(i).getId() == dbarticles
									.get(j).getId() ? true : false;
							if (findSameArticle) {
								break;
							}
						}
						if (findSameArticle == false) {
							dbManager.addArticle((articles.get(i)));
						} else {
							dbManager.updateArticleWithoutContent((articles
									.get(i)));

						}
					}

					for (int i = 0; i < dbarticles.size(); i++) {
						for (int j = 0; j < articles.size(); j++) {
							findSameArticle = articles.get(j).getId() == dbarticles
									.get(i).getId() ? true : false;
							if (findSameArticle) {
								break;
							}
						}
						if (findSameArticle == false) {
							dbManager.deleteArticle((dbarticles.get(i)));
						}
					}
				}
			}

			GetHeadTitle getHeadTitle = new GetHeadTitle();
			getHeadTitle.makeJsonToHeadTitle();
			headtitles = (ArrayList<HeadTitle>) getHeadTitle.getHeadTitles();
			for (int i = 0; i < dbheadtitles.size(); i++) {
				dbManager.deleteHeadTitle(dbheadtitles.get(i).getId());
			}
			dbManager.addHeadTitles(headtitles);
			while (!getHeadTitle.isComplete()) {

			}
			return "";
		}

		@Override
		protected void onPostExecute(String result) {
			if (magazines.size() == 0) {
				for (int i = 0; i < dbmagazines.size(); i++) {
					dbManager.deleteMagazine(dbmagazines.get(i));
				}
			} else {

				if (dbmagazines.size() == 0) {
					dbManager.addMagazines(magazines);
				} else {

					for (int i = 0; i < dbmagazines.size(); i++) {
						for (int j = 0; j < magazines.size(); j++) {
							findSameMagazine = magazines.get(j).getId() == dbmagazines
									.get(i).getId() ? true : false;
							if (findSameMagazine) {
								Mwhich = j;
								break;
							}
						}
						if (findSameMagazine == false) {
							dbManager.deleteMagazine(dbmagazines.get(i));
						} else {
							dbManager.updateMagazine(magazines.get(Mwhich));
						}

					}

					for (int i = 0; i < magazines.size(); i++) {
						for (int j = 0; j < dbmagazines.size(); j++) {
							findSameMagazine = magazines.get(i).getId() == dbmagazines
									.get(j).getId() ? true : false;
							if (findSameMagazine) {
								break;
							}
						}
						if (findSameMagazine == false) {
							dbManager.addMagazine(magazines.get(i));
						}
					}
				}
			}

			dbmagazines = dbManager.queryMagazine();
			dbheadtitles = dbManager.queryHeadTitle();
			mCategoryAdapter = new CategoryAdapter(mContext, dbmagazines);
			bindData();
			mCategoryAdapter.notifyDataSetChanged();
			mShowAdapter.notifyDataSetChanged();
			super.onPostExecute(result);
			hideRefreshAnimation();
			download.setEnabled(true);
		}
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		super.onPrepareOptionsMenu(menu);
		refresh = menu.findItem(R.id.refresh);
		download = menu.findItem(R.id.download);
		return true;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.magazine_, menu);
		return true;
	}

	@SuppressLint("CommitPrefEdits")
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.refresh) {
			download.setEnabled(false);
			showRefreshAnimation(item, REFRESH);
			MRefresh refresh = new MRefresh();
			refresh.execute("");
			return true;
		} else if (item.getItemId() == R.id.download) {
				refresh.setEnabled(false);
				Toast.makeText(mContext, "开始离线文章，时间可能过长，请您耐心等待!",
						Toast.LENGTH_SHORT).show();
				isdownloading = 1;
				showRefreshAnimation(item, DOWNLOAD);
				Refresh refresh = new Refresh();
				refresh.execute("");
				return true;				
		} else if(item.getItemId() == R.id.action_about) {
			Intent intent = new Intent(mContext, About_Activity.class);
			mContext.startActivity(intent);
			((Activity) mContext).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	private void showRefreshAnimation(MenuItem item, int type) {
		hideRefreshAnimation();

		refreshItem = item;

		// 这里使用一个ImageView设置成MenuItem的ActionView，这样我们就可以使用这个ImageView显示旋转动画了
		ImageView refreshActionView = (ImageView) getLayoutInflater().inflate(
				R.layout.action_view, null);
		if (type == DOWNLOAD) {
			refreshActionView.setImageResource(drawable.down);
		} else {
			refreshActionView.setImageResource(drawable.rotate);
		}
		refreshItem.setActionView(refreshActionView);

		// 显示刷新动画
		Animation animation = AnimationUtils.loadAnimation(this, anim.refresh);
		animation.setRepeatMode(Animation.RESTART);
		animation.setRepeatCount(Animation.INFINITE);
		refreshActionView.startAnimation(animation);
	}

	private void hideRefreshAnimation() {
		if (refreshItem != null) {
			View view = refreshItem.getActionView();
			if (view != null) {
				view.clearAnimation();
				refreshItem.setActionView(null);
			}
		}
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, Utils.FLURRY_API_KEY);
	}

	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(isdownloading == 1) {
			AlertDialog.Builder commentDialog = new AlertDialog.Builder(mContext)
				.setTitle("正在离线所有文章，确认退出？")
				.setNegativeButton("取消", null)
				.setPositiveButton("确定", null);
		final AlertDialog dialog = commentDialog.create();
		dialog.show();
		dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
				new OnClickListener() {

					@Override
					public void onClick(View v) {
						finish();
					}
				});
		}
		return super.onKeyDown(keyCode, event);
	}
}
