package org.liurongchan.young;

import java.util.ArrayList;

import org.liurongchan.adapter.MagazineAdapter;
import org.liurongchan.db.DBManager;
import org.liurongchan.model.Magazine;
import org.liurongchan.utils.Utils;

import com.flurry.android.FlurryAgent;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;
import android.widget.ListView;
import android.widget.AbsListView.OnScrollListener;

public class Magazine_Activity extends ActionBarActivity implements
		OnScrollListener {

	private ListView magazineListView;

	private MagazineAdapter mMagazineAdapter;
	private Context mContext;

	private String mCategory;
	
	private DBManager dbManager;

	private ArrayList<Magazine> dbmagazines;
	private ArrayList<Magazine> magazines;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mCategory = getIntent().getExtras().getString("Category");
		getSupportActionBar().setTitle(mCategory);
		getSupportActionBar().setLogo(R.drawable.logo_color);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		getSupportActionBar().setBackgroundDrawable(this.getBaseContext().getResources().getDrawable(R.drawable.actionbar_background));
		mContext = this;
		magazines = new ArrayList<Magazine>();
		setContentView(R.layout.magazine_layout);
		initView();
		initValidata();
		bindData();
		initListener();
	}

	private void initView() {
		magazineListView = (ListView) this.findViewById(R.id.magazineList);
	}

	private void initValidata() {
		dbManager = new DBManager(mContext);
		dbmagazines = dbManager.queryMagazine();
		for (Magazine magazine : dbmagazines) {
			if(magazine.getCategory().equals(mCategory)) {
				magazines.add(magazine);
			}
		}
		mMagazineAdapter = new MagazineAdapter(mContext, magazines);
		}

	private void bindData() {
		magazineListView.setAdapter(mMagazineAdapter);
	}

	private void initListener() {

	}

	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.magazine_2, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right); 
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onScroll(AbsListView view, int firstVisibleItem,
			int visibleItemCount, int totalItemCount) {

	}

	@Override
	public void onScrollStateChanged(AbsListView view, int scrollState) {

	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if(keyCode == KeyEvent.KEYCODE_BACK) {
			finish();
			overridePendingTransition(R.anim.in_from_left, R.anim.out_to_right);
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onStart() {
		super.onStart();
		FlurryAgent.onStartSession(this, Utils.FLURRY_API_KEY);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		FlurryAgent.onEndSession(this);
	}
	
}