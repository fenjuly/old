package org.liurongchan.json;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.liurongchan.model.Articles;
import org.liurongchan.model.Magazine;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-10-21 下午1:49:56 获取最新的magazine与article
 */
public class GetmagazineAndArticle {
	private Magazine magazine;
	private Articles article;

	private List<Magazine> magazines;

	private List<Articles> articles;

	private boolean complete = false;

	private Context mContext;
	
	private final String SHARED_MAIN = "main";
	
	private  int a_count = 0;
	

	public void initializeArcticleInstance() {
		articles = new ArrayList<Articles>();
	}
	
	public List<Magazine> getMagazines() {
		return magazines;
	}

	public List<Articles> getArticles() {
		return articles;
	}

	public GetmagazineAndArticle(Context context) {
		this.mContext = context;
	}

	public GetmagazineAndArticle() {
	}


	/**
	 * 获取magazines,与articles
	 */
	public void GetMagazinesAndArticles() {
		int articleCount = 0;
		
		magazines = new ArrayList<Magazine>();
		articles = new ArrayList<Articles>();
			String strResult = connServerForResult("http://www.appao.com/youth/magazines.json?");
	
		try {
			JSONArray magazineArray = new JSONArray(strResult);
			for (int i = 0; i < magazineArray.length(); i++) {

				JSONObject jsonObject = magazineArray.getJSONObject(i);

				int id = jsonObject.getInt("id");
				String name = jsonObject.getString("name");
				String desc = jsonObject.getString("desc");
				String thumbnail = jsonObject.getString("thumbnail");
				String thumbnail_medium = jsonObject
						.getString("thumbnail_medium");
				String thumbnail_thumb = jsonObject
						.getString("thumbnail_thumb");
				String picture = getMPicturePath(thumbnail_medium, id);
//				String picture = "";
				String categpory = jsonObject.getString("category");
				magazine = new Magazine(id, name, desc, thumbnail,
						thumbnail_medium, thumbnail_thumb, picture, categpory);
				magazines.add(magazine);

				JSONArray articleArray = jsonObject.getJSONArray("articles");
				articleCount = articleCount + articleArray.length();
				for (int j = 0; j < articleArray.length(); j++) {
					JSONObject jsonObject2 = articleArray.getJSONObject(j);
					int id2 = jsonObject2.getInt("id");
					String title = jsonObject2.getString("title");
					String summary = jsonObject2.getString("summary");
					String author = jsonObject2.getString("author");
				    String content = "";	
					String head_image = jsonObject2.getString("head_image");
					String head_image_medium = jsonObject2
							.getString("head_image_medium");
					String head_image_thumb = jsonObject2
							.getString("head_image_thumb");
					int magazine2 = id;
					// String picture2 = getAPicturePath(head_image_medium,
					// id2);
					String picture2 = "";
					String category = jsonObject2.getString("category");
					String head_picture = "";
					article = new Articles(id2, title, summary, author,
							content, head_image, head_image_medium,
							head_image_thumb, magazine2, picture2, category, head_picture);
					articles.add(article);
				}
			}
			complete = true;
			final SharedPreferences mShared = mContext.getSharedPreferences(
					SHARED_MAIN, Context.MODE_PRIVATE);
			Editor editor = mShared.edit();
			editor.putInt("count", articleCount);
			editor.commit();
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	
	
	public void getArticleContent() {
		articles = new ArrayList<Articles>();
		String strResult = connServerForResult("http://www.appao.com/youth/magazines.json?type=text");

	try {
		JSONArray magazineArray = new JSONArray(strResult);
		for (int i = 0; i < magazineArray.length(); i++) {

			JSONObject jsonObject = magazineArray.getJSONObject(i);

			int id = jsonObject.getInt("id");

			JSONArray articleArray = jsonObject.getJSONArray("articles");
			for (int j = 0; j < articleArray.length(); j++) {
				a_count++;
				JSONObject jsonObject2 = articleArray.getJSONObject(j);
				int id2 = jsonObject2.getInt("id");
				String title = jsonObject2.getString("title");
				String summary = jsonObject2.getString("summary");
				String author = jsonObject2.getString("author");
			    String content = jsonObject2.getString("content");
				String head_image = jsonObject2.getString("head_image");
				String head_image_medium = jsonObject2
						.getString("head_image_medium");
				String head_image_thumb = jsonObject2
						.getString("head_image_thumb");
				int magazine2 = id;
				 String picture2 = getAPicturePath(head_image,
				 id2);
//				String picture2 = "";
				String category = jsonObject2.getString("category");
				String head_picture = "";
				article = new Articles(id2, title, summary, author,
						content, head_image, head_image_medium,
						head_image_thumb, magazine2, picture2, category, head_picture);
				articles.add(article);
			}
		}
		complete = true;

	} catch (JSONException e) {
		e.printStackTrace();
	}
	}
	
	public void getSingleArticleContent(int magazineId) {
		complete = false;
		String strResult = connServerForResult("http://www.appao.com/youth/magazines/" + magazineId + ".json?type=text");

	try {
			JSONObject jsonObject = new JSONObject(strResult);

			int id = jsonObject.getInt("id");

			JSONArray articleArray = jsonObject.getJSONArray("articles");
			for (int j = 0; j < articleArray.length(); j++) {
				a_count++;
				JSONObject jsonObject2 = articleArray.getJSONObject(j);
				int id2 = jsonObject2.getInt("id");
				String title = jsonObject2.getString("title");
				String summary = jsonObject2.getString("summary");
				String author = jsonObject2.getString("author");
			    String content = jsonObject2.getString("content");
				String head_image = jsonObject2.getString("head_image");
				String head_image_medium = jsonObject2
						.getString("head_image_medium");
				String head_image_thumb = jsonObject2
						.getString("head_image_thumb");
				int magazine2 = id;
				 String picture2 = getAPicturePath(head_image,
				 id2);
//				String picture2 = "";
				String category = jsonObject2.getString("category");
				String head_picture = getHAPicturePath(head_image_thumb, id2);
				article = new Articles(id2, title, summary, author,
						content, head_image, head_image_medium,
						head_image_thumb, magazine2, picture2, category, head_picture);
				articles.add(article);
			}
		
		complete = true;

	} catch (JSONException e) {
		e.printStackTrace();
	}
	}

	/**
	 * 获取json数据
	 */
	private String connServerForResult(String strUrl) {
		// HttpGet对象

		HttpGet httpRequest = new HttpGet(strUrl);
		String strResul = "";
		try {
			// HttpClient对象
			HttpClient httpClient = new DefaultHttpClient();
			// 获得HttpResponse对象
			HttpResponse httpResponse = httpClient.execute(httpRequest);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// 取得返回的数据
				strResul = EntityUtils.toString(httpResponse.getEntity());
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return strResul;
	}

	public Bitmap getBitmap(String url) {

		Bitmap bitmap = null;
		try {
			HttpClient client = new DefaultHttpClient();
			URI uri = URI.create(url);
			HttpGet get = new HttpGet(uri);
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			long length = entity.getContentLength();
			Log.i("czb", " " + length);
			InputStream in = entity.getContent();
			if (in != null) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				bitmap = BitmapFactory.decodeStream(in);
				in.close();
				baos.close();
				return bitmap;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bitmap;

	}

	@SuppressLint("SdCardPath")
	public String getMPicturePath(String url, int id) {
		File mpath = new File("/sdcard/download/TheYouth/");
		if(!mpath.exists()) {
			mpath.mkdirs();
		}
		
		File file = new File("/sdcard/download/TheYouth/" + String.valueOf(id)
				+ "m" + ".jpg");
		String path = "/sdcard/download/TheYouth/" + String.valueOf(id) + "m"
				+ ".jpg";
		if (file.exists()) {
			return path;
		}
		Bitmap bitmap = getBitmap(url);
		try {
			file.createNewFile();
		} catch (Exception e) {
			e.printStackTrace();
		}

		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if(bitmap != null) {
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);			
		}
		try {
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return path;

	}

	@SuppressLint("SdCardPath")
	public String getAPicturePath(String url, int id) {
		File mpath = new File("/sdcard/download/TheYouth/");
		if(!mpath.exists()) {
			mpath.mkdirs();
		}
		File file = new File("/sdcard/download/TheYouth/" + String.valueOf(id)
				+ "a" + ".jpg");
		String path = "/sdcard/download/TheYouth/" + String.valueOf(id) + "a"
				+ ".jpg";
		if (file.exists()) {
			return path;
		}
		Bitmap bitmap = getBitmap(url);
		try {
			file.createNewFile();
		} catch (Exception e) {
			e.printStackTrace();
		}

		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if(bitmap != null) {
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);			
		}
		try {
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return path;

	}
	
	@SuppressLint("SdCardPath")
	public String getHAPicturePath(String url, int id) {
		Bitmap bitmap = getBitmap(url);
		File mpath = new File("/sdcard/download/TheYouth/");
		if(!mpath.exists()) {
			mpath.mkdirs();
		}
		File file = new File("/sdcard/download/TheYouth/" + String.valueOf(id)
				+ "ha" + ".jpg");
		String path = "/sdcard/download/TheYouth/" + String.valueOf(id) + "ha"
				+ ".jpg";
		if (file.exists()) {
			return path;
		}
		try {
			file.createNewFile();
		} catch (Exception e) {
			e.printStackTrace();
		}

		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if(bitmap != null) {
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);			
		}
		try {
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return path;

	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public int getA_count() {
		return a_count;
	}

	public void setA_count(int a_count) {
		this.a_count = a_count;
	}
}
