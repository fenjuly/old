package org.liurongchan.json;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.liurongchan.model.Comment;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-10-21 下午1:55:10 获取某篇文章的评论
 */
public class GetComment {

	private Comment mComment;
	private List<Comment> mComments;
	private int magazineId;
	private int articleId;
	private boolean complete = false;

	public GetComment(int magazineId, int articleId) {
		this.articleId = articleId;
		this.magazineId = magazineId;
	}

	public void makeJsonToComment() {
		mComments = new ArrayList<Comment>();
		String strResult = connServerForResult("http://www.appao.com/youth/magazines/"
				+ magazineId
				+ "/articles/"
				+ articleId
				+ ".json?extract_comments=1");
		try {
			JSONObject article = new JSONObject(strResult);
			JSONArray comments = article.getJSONArray("comments");
			for (int i = 0; i < comments.length(); i++) {
				JSONObject comment = comments.getJSONObject(i);
				int id = 1;
				String commenter = comment.getString("commenter");
				String content = comment.getString("content");
				mComment = new Comment(id, commenter, content, articleId);
				mComments.add(mComment);
			}
			complete = true;
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}

	/**
	 * 获取json数据
	 */
	private String connServerForResult(String strUrl) {
		// HttpGet对象

		HttpGet httpRequest = new HttpGet(strUrl);
		String strResul = "";
		try {
			// HttpClient对象
			HttpClient httpClient = new DefaultHttpClient();
			// 获得HttpResponse对象
			HttpResponse httpResponse = httpClient.execute(httpRequest);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// 取得返回的数据
				strResul = EntityUtils.toString(httpResponse.getEntity());
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return strResul;
	}

	public List<Comment> getComments() {
		return mComments;
	}

	public void setComments(List<Comment> mComments) {
		this.mComments = mComments;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}
}
