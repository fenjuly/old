package org.liurongchan.json;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.liurongchan.model.HeadTitle;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-10-21 下午1:56:03 获取头条
 */
public class GetHeadTitle {

	private HeadTitle headTitle;
	private List<HeadTitle> headTitles;

	private boolean complete = false;

	public List<HeadTitle> getHeadTitles() {
		return headTitles;
	}

	public void makeJsonToHeadTitle() {
		headTitles = new ArrayList<HeadTitle>();
		String strResult = connServerForResult("http://www.appao.com/youth/covers.json");
		try {
			JSONArray headTitleArray = new JSONArray(strResult);
			for (int i = 0; i < headTitleArray.length(); i++) {
				JSONObject headTitleObj = headTitleArray.getJSONObject(i);
				
				int id = headTitleObj.getInt("id");
				String image = headTitleObj.getString("image");
				String picture = getHPicturePath(image, id);
				JSONObject playload = headTitleObj.getJSONObject("payload");
				String type = playload.getString("type");
				String value = null;
				if (type.equals("magazine") || type.equals("article")) {
				int	valueInteger = (Integer)playload.getInt("value");
					value = String.valueOf(valueInteger);
				} else {
					value  = (String)playload.getString("value");
				}
				headTitle = new HeadTitle(id, image, picture, type, value);
				headTitles.add(headTitle);
			}
			complete = true;
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 获取json数据
	 */
	private String connServerForResult(String strUrl) {
		// HttpGet对象

		HttpGet httpRequest = new HttpGet(strUrl);
		String strResul = "";
		try {
			// HttpClient对象
			HttpClient httpClient = new DefaultHttpClient();
			// 获得HttpResponse对象
			HttpResponse httpResponse = httpClient.execute(httpRequest);
			if (httpResponse.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
				// 取得返回的数据
				strResul = EntityUtils.toString(httpResponse.getEntity());
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return strResul;
	}

	public Bitmap getBitmap(String url) {

		Bitmap bitmap = null;
		try {
			HttpClient client = new DefaultHttpClient();
			URI uri = URI.create(url);
			HttpGet get = new HttpGet(uri);
			HttpResponse response = client.execute(get);
			HttpEntity entity = response.getEntity();
			long length = entity.getContentLength();
			Log.i("czb", " " + length);
			InputStream in = entity.getContent();
			if (in != null) {
				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				bitmap = BitmapFactory.decodeStream(in);
				in.close();
				baos.close();
				return bitmap;
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return bitmap;

	}

	@SuppressLint("SdCardPath")
	public String getHPicturePath(String url, int id) {
		File mpath = new File("/sdcard/download/TheYouth/");
		if(!mpath.exists()) {
			mpath.mkdirs();
		}
		File file = new File("/sdcard/download/TheYouth/" + String.valueOf(id)
				+ "h" + ".jpg");
		String path = "/sdcard/download/TheYouth/" + String.valueOf(id) + "h"
				+ ".jpg";
		if (file.exists()) {
			return path;
		}
		Bitmap bitmap = getBitmap(url);
		try {
			file.createNewFile();
		} catch (Exception e) {
			e.printStackTrace();
		}

		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		if(bitmap != null) {
			bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);			
		}
		try {
			fos.flush();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return path;

	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

}
