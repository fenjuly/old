package org.liurongchan.item;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-10-21 下午9:19:10 文章显示列表分类和内容的接口
 */
public interface ListItems {
	public int getLayout();

	public boolean isClickable();

	public View getView(Context context, View convertView,
			LayoutInflater inflater, ViewGroup parent);

}
