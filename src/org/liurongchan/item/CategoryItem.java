package org.liurongchan.item;

import org.liurongchan.young.R;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-10-21 下午9:21:20
 * 
 */
public class CategoryItem implements ListItems {

	private String mLabel;
	private Typeface tf;

	public CategoryItem(String label, Context mContext) {
		this.mLabel = label;
		tf = Typeface.createFromAsset(mContext.getAssets(),
				"fonts/Roboto-Bold.ttf");
	}

	@Override
	public int getLayout() {
		return R.layout.label_layout;
	}

	@Override
	public boolean isClickable() {
		return false;
	}

	@Override
	public View getView(Context context, View convertView,
			LayoutInflater inflater, ViewGroup parent) {
		convertView = inflater.inflate(getLayout(), null);
		TextView category = (TextView) convertView.findViewById(R.id.category);
		category.setTypeface(tf);
		category.setText(mLabel);
		return convertView;
	}

}
