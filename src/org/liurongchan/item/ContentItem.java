package org.liurongchan.item;


import java.io.File;

import org.liurongchan.listener.ArticleItemListener;
import org.liurongchan.young.R;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;
import com.squareup.picasso.Picasso.LoadedFrom;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnLongClickListener;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-10-21 下午9:29:28 类说明
 */
public class ContentItem implements ListItems {

	private String title;
	private String summary;
	private String picurl;
	private String path;
	private int articleId;

	private Context mContext;

	public ContentItem(String path, String picurl, String title, String summary,
			int articleId, Context mContext) {
		this.path = path;
		this.title = title;
		this.picurl = picurl;
		this.summary = summary;
		this.articleId = articleId;
		this.mContext = mContext;
	}

	@Override
	public int getLayout() {
		return R.layout.item_article;
	}

	@Override
	public boolean isClickable() {
		return true;
	}

	@Override
	public View getView(Context context, View convertView,
			LayoutInflater mLayoutInflater, ViewGroup parent) {
		TextView summaryTextView;
		TextView titleTextView;
		final ImageView thumbImageView;
		// ViewHolder holder;
		// if (convertView == null) {
		convertView = mLayoutInflater.inflate(getLayout(), null);
		Typeface mRobotoTitle = Typeface.createFromAsset(mContext.getAssets(),
				"fonts/Roboto-Bold.ttf");
		titleTextView = (TextView) convertView.findViewById(R.id.title);
		titleTextView.setTypeface(mRobotoTitle);
		summaryTextView = (TextView) convertView.findViewById(R.id.summary);
		thumbImageView = (ImageView) convertView.findViewById(R.id.article_img);
		// holder = new ViewHolder(titleTextView, summaryTextView,
		// thumbImageView);
		// convertView.setTag(holder);
		// } else {
		// holder = (ViewHolder) convertView.getTag();
		//
		// titleTextView = holder.titleTextView;
		// summaryTextView = holder.summaryTextView;
		// thumbImageView = holder.thumbImageView;
		//
		// }
		if(path.equals("")) {
			Picasso.with(mContext).load(picurl)
			.error(R.drawable.placeholder_fail).into(new Target() {
				@Override
				public void onBitmapLoaded(Bitmap bitmap,
						LoadedFrom loadedFrom) {
					if(bitmap == null) {
						thumbImageView.setImageResource(R.drawable.placeholder_fail);
					}else {
						float ratio = (float) bitmap.getHeight()
							/ (float) bitmap.getWidth();
						float widthFloat = 120;
						float heightFloat = widthFloat * ratio;
						Bitmap bmp = Bitmap.createScaledBitmap(bitmap, (int)widthFloat, (int)heightFloat, true);
						thumbImageView.setImageBitmap(ImageCrop(bmp));
						thumbImageView.invalidate();
					}
				}

				@Override
				public void onBitmapFailed() {
				}

			});

		} else {
			Picasso.with(mContext).load(new File(path))
			.error(R.drawable.placeholder_fail).into(new Target() {
				@Override
				public void onBitmapLoaded(Bitmap bitmap,
						LoadedFrom loadedFrom) {

					if(bitmap == null) {
						thumbImageView.setImageResource(R.drawable.placeholder_fail);
					}else {
						float ratio = (float) bitmap.getHeight()
							/ (float) bitmap.getWidth();
						float widthFloat = 120;
						float heightFloat = widthFloat * ratio;
						Bitmap bmp = Bitmap.createScaledBitmap(bitmap, (int)widthFloat, (int)heightFloat, true);
						thumbImageView.setImageBitmap(ImageCrop(bmp));
						thumbImageView.invalidate();
					}
				}

				@Override
				public void onBitmapFailed() {
				}

			});			
		}
		titleTextView.setText(title);
		summaryTextView.setText(summary);
		convertView.setOnClickListener(new ArticleItemListener(mContext,
				articleId));
		// convertView.setOnClickListener(new VideoListItemListener(mContext,
		// this, video));
		convertView.setOnLongClickListener(new OnLongClickListener() {
			// 保证长按事件传递
			@Override
			public boolean onLongClick(View v) {
				return false;
			}
		});
		return convertView;
	}

	// private static class ViewHolder {
	// public TextView summaryTextView;
	// public TextView titleTextView;
	// public ImageView thumbImageView;
	//
	// public ViewHolder(TextView title, TextView content, ImageView image) {
	// summaryTextView = content;
	// titleTextView = title;
	// thumbImageView = image;
	// }
	// }

	public  Bitmap ImageCrop(Bitmap bitmap) {
        int h = bitmap.getHeight();

        Bitmap bmp = null;
		int ww = 120;// 裁切后所取的正方形区域边长
        int wh = 120;
        if(h > 120){
        	bmp = bitmap;
        } else {
           bmp = Bitmap.createScaledBitmap(bitmap, (int)ww, (int)wh, true);
        }

        int retX = 0;
        int retY = 0;

        //下面这句是关键
        return Bitmap.createBitmap(bmp, retX, retY, ww, wh, null, false);
    }
}
