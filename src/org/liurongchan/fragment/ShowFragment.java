package org.liurongchan.fragment;


import org.liurongchan.listener.HeadItemListener;
import org.liurongchan.model.HeadTitle;
import org.liurongchan.young.R;


import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


@SuppressLint("ValidFragment")
public class ShowFragment extends Fragment {

	private ImageView mShowImageView;
	private float widthFloat;
	private float heightFloat;
	private Bitmap bmp;
	public ShowFragment() {
	}

	public static ShowFragment newInstance(HeadTitle headInfo) {
		ShowFragment f = new ShowFragment();
		Bundle args = new Bundle();
		args.putSerializable("head_info", headInfo);
		f.setArguments(args);
		return f;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		final View layout = inflater
				.inflate(R.layout.fragment_show, container, false);
		mShowImageView = (ImageView) layout.findViewById(R.id.show_image);
//		mShowImageView.setImageResource(R.drawable.big_bg);
		HeadTitle mHeadInfo = (HeadTitle) getArguments()
				.getSerializable("head_info");
//		Picasso.with(layout.getContext())
//				.load(new File(mHeadInfo.getPicture())).placeholder(R.drawable.big_bg)
//				.error(R.drawable.big_bg).into(new Target() {
//					@Override
//					public void onBitmapLoaded(Bitmap bitmap,
//							LoadedFrom loadedFrom) {
//						float ratio = (float) bitmap.getHeight()
//						/ (float) bitmap.getWidth();
//				widthFloat = (float) getActivity().getWindowManager().getDefaultDisplay().getWidth()/2;
//				heightFloat = ((float) getActivity().getWindowManager().getDefaultDisplay().getWidth())
//						* ratio/2;
//				bmp = Bitmap.createScaledBitmap(bitmap, (int)widthFloat, (int)heightFloat, true);
//				bmp = ImageCrop(bmp);
//				mShowImageView.setImageBitmap(ImageCrop(bmp));
//				}
//
//					@Override
//					public void onBitmapFailed() {
//					}
//
//				});
		bmp =  BitmapFactory.decodeFile(mHeadInfo.getPicture());
		if(bmp == null) {
			mShowImageView.setImageResource(R.drawable.big_bg);
		}else {			
			float ratio = (float) bmp.getHeight()
					/ (float) bmp.getWidth();
			widthFloat = 360;
			heightFloat = widthFloat * ratio;
			bmp = Bitmap.createScaledBitmap(bmp, (int)widthFloat, (int)heightFloat, true);
			bmp = ImageCrop(bmp);
			mShowImageView.setImageBitmap(bmp);
		}
		mShowImageView.setTag(mHeadInfo);
		mShowImageView.setOnClickListener(new HeadItemListener(
				getActivity(), mHeadInfo));
		
		return layout;
	}

	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}
	
	public  Bitmap ImageCrop(Bitmap bitmap) {
        int h = bitmap.getHeight();
        Bitmap bmp = null;
		int ww = 360;// 裁切后所取的正方形区域边长
        int wh = 202;
        if(h > 202){
        	bmp = bitmap;
        } else {
           bmp = Bitmap.createScaledBitmap(bitmap, (int)ww, (int)202, true);
        }

        int retX = 0;
        int retY = 0;

        //下面这句是关键
        return Bitmap.createBitmap(bmp, retX, retY, ww, wh, null, false);
    }
	
}
