package org.liurongchan.adapter;

import java.util.ArrayList;

import org.liurongchan.listener.MagazineItemListener;
import org.liurongchan.model.Magazine;
import org.liurongchan.young.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


public class MagazineAdapter extends BaseAdapter {
	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private final Typeface mRobotoTitle;
	private ArrayList<Magazine> mMagazineList;
	private  ImageView thumbImageView;

	public MagazineAdapter(Context context, ArrayList<Magazine> magazineList) {
		mRobotoTitle = Typeface.createFromAsset(context.getAssets(),
				"fonts/Roboto-Bold.ttf");
		mContext = context;
		mLayoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mMagazineList = magazineList;

	}

	@Override
	public int getCount() {
		return mMagazineList.size();
	}

	@Override
	public Object getItem(int position) {
		return mMagazineList.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView titleTextView;
		TextView contentTextView;
		Bitmap bmp;
//		ImageView thumbImageView;
		ViewHolder holder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.magazine_item,
					parent, false);
			titleTextView = (TextView) convertView.findViewById(R.id.title);
			titleTextView.setTypeface(mRobotoTitle);
			contentTextView = (TextView) convertView.findViewById(R.id.content);
			thumbImageView = (ImageView) convertView.findViewById(R.id.thumb);
			holder = new ViewHolder(titleTextView, contentTextView,
					thumbImageView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
			titleTextView = holder.titleText;
			contentTextView = holder.contentText;
			thumbImageView = holder.thumbImageView;
		}

		Magazine magazine = (Magazine) getItem(position);
//		Picasso.with(mContext).load(new File(magazine.getPicture()))
//				.placeholder(R.drawable.placeholder_thumb)
//				.error(R.drawable.placeholder_fail).into(new Target() {
//					@Override
//					public void onBitmapLoaded(Bitmap bitmap,
//							LoadedFrom loadedFrom) {
//
//						float ratio = (float) bitmap.getHeight()
//								/ (float) bitmap.getWidth();
//						float widthFloat = 40;
//						float heightFloat = widthFloat * ratio;
//						Bitmap bmp = Bitmap.createScaledBitmap(bitmap, (int)widthFloat, (int)heightFloat, true);
//						thumbImageView.setImageBitmap(ImageCrop(bmp));
//						thumbImageView.invalidate();
//					}
//
//					@Override
//					public void onBitmapFailed() {
//					}
//
//				});
		bmp =  BitmapFactory.decodeFile(magazine.getPicture());
//		float ratio = (float) bmp.getHeight()
//				/ (float) bmp.getWidth();
//		float widthFloat = 40;
//		float heightFloat = widthFloat * ratio;
//		bmp = Bitmap.createScaledBitmap(bmp, (int)widthFloat, (int)heightFloat, true);
//		bmp = ImageCrop(bmp);
		if(bmp == null){
			thumbImageView.setImageResource(R.drawable.big_bg);
		} else {
			thumbImageView.setImageBitmap(bmp);
		}
		titleTextView.setText(magazine.getName());
		contentTextView.setText(magazine.getDesc());
		convertView.setOnClickListener(new MagazineItemListener(mContext,
				mMagazineList.get(position).getId()));
		convertView.setOnLongClickListener(new OnLongClickListener() {
			// 保证长按事件传递
			@Override
			public boolean onLongClick(View v) {
				return false;
			}
		});
		return convertView;
	}

	private static class ViewHolder {
		public TextView titleText;
		public TextView contentText;
		public ImageView thumbImageView;

		public ViewHolder(TextView title, TextView content, ImageView image) {
			titleText = title;
			contentText = content;
			thumbImageView = image;
		}
	}

	public  Bitmap ImageCrop(Bitmap bitmap) {
        int h = bitmap.getHeight();

        Bitmap bmp = null;
        int ww = 40;
        int wh = 120;
        if(h > 120){                                                                                            
        	bmp = bitmap;
        } else {
           bmp = Bitmap.createScaledBitmap(bitmap, (int)ww, (int)120, true);
        }

        int retX = 0;
        int retY = 0;

        //下面这句是关键
        return Bitmap.createBitmap(bmp, retX, retY, ww, wh, null, false);
    }
	
}
