package org.liurongchan.adapter;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.liurongchan.listener.CategoryItemListener;
import org.liurongchan.model.Magazine;
import org.liurongchan.young.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnLongClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-11 下午11:16:30 类说明
 */
public class CategoryAdapter extends BaseAdapter {

	private Context mContext;
	private LayoutInflater mLayoutInflater;
	private final Typeface mRobotoTitle;
	private ArrayList<String> mCategories;
	private boolean findSameCategory = false;
	
	private HashMap<String, Integer> categor_imgs;

	public CategoryAdapter(Context context, ArrayList<Magazine> magazineList) {
		mRobotoTitle = Typeface.createFromAsset(context.getAssets(),
				"fonts/Roboto-Bold.ttf");
		mContext = context;
		mLayoutInflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		mCategories = new ArrayList<String>();
		categor_imgs = new HashMap<String, Integer>();
			categor_imgs.put("乐活", R.drawable.lehuo);
			categor_imgs.put("关注", R.drawable.guanzhu);
			categor_imgs.put("阅读", R.drawable.qingxiaoshuo);
		for (int i = 0; i < magazineList.size(); i++) {
			if (i == 0) {
				mCategories.add(magazineList.get(0).getCategory());
			}
			for (int j = 0; j < mCategories.size(); j++) {
				if (mCategories.get(j)
						.equals(magazineList.get(i).getCategory()) && i != 0) {
					findSameCategory = true;
				}
			}
			if (!findSameCategory && i != 0) {
				mCategories.add(magazineList.get(i).getCategory());
			}
			findSameCategory = false;
		}
	}

	@Override
	public int getCount() {
		return mCategories.size();
	}

	@Override
	public Object getItem(int position) {
		return mCategories.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		TextView categoryText;
		ImageView categoryImage;
		Bitmap bitmap;
		ViewHolder holder;
		if (convertView == null) {
			convertView = mLayoutInflater.inflate(R.layout.category_item,
					parent, false);
			categoryText = (TextView) convertView.findViewById(R.id.category);
			categoryImage = (ImageView) convertView
					.findViewById(R.id.category_thumb);
			bitmap = BitmapFactory.decodeResource(mContext.getResources(),
					R.drawable.beiyong);
			Iterator<String> iterator = categor_imgs.keySet().iterator();
			while(iterator.hasNext()) {
				String categoryString = (String) iterator.next(); 
					if(mCategories.get(position).equals(categoryString)) {
						bitmap = BitmapFactory.decodeResource(mContext.getResources(),
								categor_imgs.get(categoryString));
					}
			}
			if(bitmap == null) {
				categoryImage.setImageResource(R.drawable.big_bg);
			}else {
				categoryImage.setImageBitmap(bitmap);
			}
			categoryText.setTypeface(mRobotoTitle);
			holder = new ViewHolder(categoryText, categoryImage);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
			categoryText = holder.categoryText;
			categoryImage = holder.categoryImage;
		}
		categoryText.setText(mCategories.get(position));
		convertView.setOnClickListener(new CategoryItemListener(mContext,
				mCategories.get(position)));
		convertView.setOnLongClickListener(new OnLongClickListener() {
			// 保证长按事件传递
			@Override
			public boolean onLongClick(View v) {
				return false;
			}
		});
		return convertView;
	}

	private static class ViewHolder {
		public TextView categoryText;
		public ImageView categoryImage;;

		public ViewHolder(TextView category, ImageView categoryImage) {
			categoryText = category;
			this.categoryImage = categoryImage;
		}
	}

}
