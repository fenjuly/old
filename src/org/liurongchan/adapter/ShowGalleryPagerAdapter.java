package org.liurongchan.adapter;

import java.util.ArrayList;

import org.liurongchan.fragment.ShowFragment;
import org.liurongchan.model.HeadTitle;


import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

public class ShowGalleryPagerAdapter extends FragmentPagerAdapter {

	private ArrayList<HeadTitle> mShow = new ArrayList<HeadTitle>();

	public ShowGalleryPagerAdapter(FragmentManager fm,
			ArrayList<HeadTitle> HeadShowList) {
		super(fm);
		mShow = HeadShowList;
	}

	private ShowGalleryPagerAdapter(FragmentManager fm) {
		super(fm);
	}

	@Override
	public Fragment getItem(int position) {
		return ShowFragment.newInstance(mShow.get(position));
	}

	@Override
	public int getCount() {
		return mShow.size();
	}

}
