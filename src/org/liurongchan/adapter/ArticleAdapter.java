package org.liurongchan.adapter;

import java.util.List;

import org.liurongchan.item.ListItems;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

/**
 * @author [FeN]July  E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-8 下午9:07:08
 * 类说明
 */
public class ArticleAdapter extends BaseAdapter {

	private List<ListItems> mListItems;
	private Context mContext;
	private LayoutInflater inflater;

	public ArticleAdapter(List<ListItems> mListItems, Context mContext, LayoutInflater inflater) {
		this.mListItems = mListItems;
		this.mContext = mContext;
		this.inflater = inflater;
	}

	@Override
	public int getCount() {
		return mListItems.size();
	}

	@Override
	public Object getItem(int position) {
		return mListItems.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public boolean isEnabled(int position) {
		return mListItems.get(position).isClickable();

	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		return mListItems.get(position).getView(mContext, convertView, inflater, parent);
	}

}