package org.liurongchan.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-9-27 下午1:47:31 维护和管理数据库的基类
 */
public class DBHelper extends SQLiteOpenHelper {

	public static final String DATABASE_NAME = "youth.db";
	public static final int DATABASE_VERSION = 1;

	public DBHelper(Context context) {
		// CusorFactory设置为null，使用默认值
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	// 数据库第一次被创建时onCreate会被调用
	@Override
	public void onCreate(SQLiteDatabase db) {

		// 创建magazine与articles表，如果不存在的话
		db.execSQL("CREATE TABLE IF NOT EXISTS magazine"
				+ "(_id INTEGER PRIMARY KEY, name VARCHAR, desc TEXT, thumbnail VARCHAR, thumbnail_medium VARCHAR, thumbnail_thumb VARCHAR, picture VARCHAR, category VARCHAR)");
		db.execSQL("CREATE TABLE IF NOT EXISTS articles"
				+ "(_id INTEGER PRIMARY KEY, title VARCAHR, summary TEXT, author VARCHAR, content TEXT, head_image VARCHAR, head_image_medium VARCHAR, head_image_thumb VARCHAR, magazine INTEGER, picture VARCHAR, category VARCHAR, head_picture VARCHAR)");
		db.execSQL("CREATE TABLE IF NOT EXISTS headtitle"
				+ "(_id INTEGER PRIMARY KEY, image VARCHAR, picture VARCHAR, type VARCHAR, value VARCHAR)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("ALTER TABLE magazine ADD COLUMN other STRING");
		db.execSQL("ALTER TABLE articles ADD COLUMN other STRING");
	}

}
