package org.liurongchan.db;

import java.util.ArrayList;

import org.liurongchan.model.Articles;
import org.liurongchan.model.HeadTitle;
import org.liurongchan.model.Magazine;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-9-27 下午2:21:03 封装所有的业务方法
 */
public class DBManager {

	private DBHelper helper;
	private SQLiteDatabase db;

	public DBManager(Context context) {
		helper = new DBHelper(context);
		// 因为getWritableDatabase内部调用了mContext.openOrCreateDatabase(mName, 0,
		// mFactory);
		// 所以要确保context已初始化,我们可以把实例化DBManager的步骤放在Activity的onCreate里
		db = helper.getWritableDatabase();
	}

	public void addMagazine(Magazine magazine) {
		db.beginTransaction();// 开始事务

		try {
			db.execSQL(
					"INSERT INTO magazine VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
					new Object[] { magazine.getId(), magazine.getName(),
							magazine.getDesc(), magazine.getThumbnail(),
							magazine.getThumbnail_medium(),
							magazine.getThumbnail_thumb(),
							magazine.getPicture(), magazine.getCategory() });
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public void addMagazines(ArrayList<Magazine> magazines) {
		db.beginTransaction();// 开始事务
		try {
			for (Magazine magazine : magazines) {
				db.execSQL(
						"INSERT INTO magazine VALUES(?, ?, ?, ?, ?, ?, ?, ?)",
						new Object[] { magazine.getId(), magazine.getName(),
								magazine.getDesc(), magazine.getThumbnail(),
								magazine.getThumbnail_medium(),
								magazine.getThumbnail_thumb(),
								magazine.getPicture(), magazine.getCategory() });
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public void updateMagazine(Magazine magazine) {
		db.beginTransaction();
		try {
			ContentValues cv = new ContentValues();
			cv.put("name", magazine.getName());
			cv.put("desc", magazine.getDesc());
			cv.put("thumbnail", magazine.getThumbnail());
			cv.put("thumbnail_medium", magazine.getThumbnail_medium());
			cv.put("thumbnail_thumb", magazine.getThumbnail_thumb());
			cv.put("picture", magazine.getPicture());
			cv.put("category", magazine.getCategory());
			db.update("magazine", cv, "_id=?",
					new String[] { String.valueOf(magazine.getId()) });
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public void deleteMagazine(Magazine magazine) {
		db.delete("magazine", "_id=?",
				new String[] { String.valueOf(magazine.getId()) });
	}

	public ArrayList<Integer> getMagazineId() {
		ArrayList<Integer> magazineIds = new ArrayList<Integer>();
		Cursor c = queryTheMagazineCursor();
		while (c.moveToNext()) {
			int id = c.getInt(c.getColumnIndex("_id"));
			magazineIds.add(id);
		}
		return magazineIds;

	}

	public ArrayList<Magazine> queryMagazine() {
		ArrayList<Magazine> magazines = new ArrayList<Magazine>();
		Cursor c = queryTheMagazineCursor();

		while (c.moveToNext()) {
			int id = c.getInt(0);
			String name = c.getString(1);
			String desc = c.getString(2);
			String thumbnail = c.getString(3);
			String thumbnail_medium = c.getString(4);
			String thumbnail_thumb = c.getString(5);
			String picture = c.getString(6);
			String category = c.getString(7);
			Magazine magazine = new Magazine(id, name, desc, thumbnail,
					thumbnail_medium, thumbnail_thumb, picture, category);
			magazines.add(magazine);
		}
		c.close();
		return magazines;
	}
	
	public Magazine queryMagazineById(int id) {
		Cursor c = queryMagazineByIdCursor(id);
		Magazine magazine = null;
		while (c.moveToNext()) {
			String name = c.getString(1);
			String desc = c.getString(2);
			String thumbnail = c.getString(3);
			String thumbnail_medium = c.getString(4);
			String thumbnail_thumb = c.getString(5);
			String picture = c.getString(6);
			String category = c.getString(7);
			magazine = new Magazine(id, name, desc, thumbnail,
					thumbnail_medium, thumbnail_thumb, picture, category);
		}
		return magazine;
	}

	public void addArticle(Articles article) {
		db.beginTransaction();// 开始事务
		try {
			db.execSQL(
					"INSERT INTO articles VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)",
					new Object[] { article.getId(), article.getTitle(),
							article.getSummary(), article.getAuthor(),
							article.getContent(), article.getHead_image(),
							article.getHead_image_medium(),
							article.getHead_image_thumb(),
							article.getMagazine(), article.getPicture(),
							article.getCategory(), article.getHead_picture() });
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public void addArticles(ArrayList<Articles> articles) {
		db.beginTransaction();// 开始事务
		try {
			for (Articles article : articles) {
				db.execSQL(
						"INSERT INTO articles VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
						new Object[] { article.getId(), article.getTitle(),
								article.getSummary(), article.getAuthor(),
								article.getContent(), article.getHead_image(),
								article.getHead_image_medium(),
								article.getHead_image_thumb(),
								article.getMagazine(), article.getPicture(),
								article.getCategory(), article.getHead_picture() });
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public void updateArticle(Articles article) {
		db.beginTransaction();
		try {
			ContentValues cv = new ContentValues();
			cv.put("content", article.getContent());
			cv.put("picture", article.getPicture());
			cv.put("head_picture", article.getHead_picture());
			db.update("articles", cv, "_id=?",
					new String[] { String.valueOf(article.getId()) });
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}
	public void updateArticleWithoutContent(Articles article) {
		db.beginTransaction();
		try {
			ContentValues cv = new ContentValues();
			cv.put("title", article.getTitle());
			cv.put("summary", article.getSummary());
			cv.put("author", article.getAuthor());
			cv.put("head_image", article.getHead_image());
			cv.put("head_image_medium", article.getHead_image_medium());
			cv.put("head_image_thumb", article.getHead_image_thumb());
			cv.put("magazine", article.getMagazine());
			cv.put("category", article.getCategory());
			db.update("articles", cv, "_id=?",
					new String[] { String.valueOf(article.getId()) });
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}
	

	public void deleteArticle(Articles article) {
		db.delete("articles", "_id=?",
				new String[] { String.valueOf(article.getId()) });
	}

	public ArrayList<Integer> getArticleId() {
		ArrayList<Integer> articleIds = new ArrayList<Integer>();
		Cursor c = queryTheArticlesCursor();
		while (c.moveToNext()) {
			int id = c.getInt(c.getColumnIndex("_id"));
			articleIds.add(id);
		}
		return articleIds;

	}

	public int getMagazineID(int articleId) {

		Cursor c = db.rawQuery("SELECT magazine FROM articles where _id="
				+ articleId, null);
		int magazineId = 0;
		while (c.moveToNext()) {
			magazineId = c.getInt(0);
		}
		return magazineId;
	}

	public ArrayList<Articles> queryArticle() {
		ArrayList<Articles> articles = new ArrayList<Articles>();
		Cursor c = queryTheArticlesCursor();
		while (c.moveToNext()) {
			int id = c.getInt(0);
			String title = c.getString(1);
			String summary = c.getString(2);
			String author = c.getString(3);
			String content = c.getString(4);
			String head_image = c.getString(5);
			String head_image_medium = c.getString(6);
			String head_image_thumb = c.getString(7);
			int magazine = c.getInt(8);
			String picture = c.getString(9);
			String category = c.getString(10);
			String head_picture = c.getString(11);
			Articles article = new Articles(id, title, summary, author,
					content, head_image, head_image_medium, head_image_thumb,
					magazine, picture, category, head_picture);
			articles.add(article);
		}
		c.close();
		return articles;
	}

	public Articles queryArticleById(int articleId) {

		Cursor c = db.rawQuery("SELECT * FROM articles where _id=" + articleId,
				null);
		Articles article = null;
		while (c.moveToNext()) {
			int id = c.getInt(0);
			String title = c.getString(1);
			String summary = c.getString(2);
			String author = c.getString(3);
			String content = c.getString(4);
			String head_image = c.getString(5);
			String head_image_medium = c.getString(6);
			String head_image_thumb = c.getString(7);
			int magazine = c.getInt(8);
			String picture = c.getString(9);
			String category = c.getString(10);
			String head_picture = c.getString(11);
			article = new Articles(id, title, summary, author, content,
					head_image, head_image_medium, head_image_thumb, magazine,
					picture, category, head_picture);
		}
		return article;
	}

	public void addHeadTitles(ArrayList<HeadTitle> headTitles) {
		db.beginTransaction();// 开始事务
		try {
			for (HeadTitle headtitle : headTitles) {
				db.execSQL("INSERT INTO headtitle VALUES(?, ?, ?, ?, ?)",
						new Object[] { headtitle.getId(), headtitle.getImage(),
								headtitle.getPicture(), headtitle.getType(),
								headtitle.getValue() });
			}
			db.setTransactionSuccessful();
		} finally {
			db.endTransaction();
		}
	}

	public void deleteHeadTitle(int id) {
		db.delete("headtitle", "_id=?", new String[] { String.valueOf(id) });
	}

	public ArrayList<HeadTitle> queryHeadTitle() {
		ArrayList<HeadTitle> headTitles = new ArrayList<HeadTitle>();
		Cursor c = queryTheHeadTitleCursor();
		while (c.moveToNext()) {
			int id = c.getInt(0);
			String image = c.getString(1);
			String picture = c.getString(2);
			String type = c.getString(3);
			String value = c.getString(4);
			HeadTitle headtitle = new HeadTitle(id, image, picture, type, value);
			headTitles.add(headtitle);
		}
		c.close();
		return headTitles;
	}

	public Cursor queryTheArticlesCursor() {
		Cursor c = db.rawQuery("SELECT * FROM articles", null);
		return c;
	}
	
	public Cursor queryMagazineByIdCursor(int id) {
		Cursor c =db.rawQuery("SELECT * FROM magazine WHERE _id=" + id, null);
		return c;
	}
	public Cursor queryTheMagazineCursor() {
		Cursor c = db.rawQuery("SELECT * FROM magazine", null);
		return c;
	}

	public Cursor queryTheHeadTitleCursor() {
		Cursor c = db.rawQuery("SELECT * FROM headtitle", null);
		return c;
	}

	public void closeDB() {
		db.close();
	}
}
