package org.liurongchan.listener;

import org.liurongchan.young.Content_Activity;
import org.liurongchan.young.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * @author [FeN]July  E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-10 下午10:30:17
 * 类说明
 */
public class ArticleItemListener implements OnClickListener {

	private int articleId;
	private Context mContext;
	
	public ArticleItemListener(Context context, int articleId) {
		this.articleId = articleId;
		mContext = context;
	}

	@Override
	public void onClick(View arg0) {
		Intent intent = new Intent(mContext, Content_Activity.class);
		intent.putExtra("ArticleID", articleId);
		mContext.startActivity(intent);
		((Activity) mContext).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left); 
	}

}
