package org.liurongchan.listener;

import org.liurongchan.young.Magazine_Activity;
import org.liurongchan.young.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * @author [FeN]July  E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-11 下午11:31:53
 * 类说明
 */
public class CategoryItemListener implements OnClickListener {

	private String mCategory;
	private Context mContext;
	
	public CategoryItemListener(Context context, String category) {
		mContext = context;
		mCategory = category;
	}
	
	@Override
	public void onClick(View arg0) {
		Intent intent = new Intent(mContext, Magazine_Activity.class);
		intent.putExtra("Category", mCategory);
		mContext.startActivity(intent);
		((Activity) mContext).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left); 
	}

}
