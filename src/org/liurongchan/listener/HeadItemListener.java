package org.liurongchan.listener;

import org.liurongchan.model.HeadTitle;
import org.liurongchan.young.Article_Activity;
import org.liurongchan.young.Content_Activity;
import org.liurongchan.young.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * @author [FeN]July  E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-11 下午5:12:21
 * 类说明
 */
public class HeadItemListener implements OnClickListener {

	private HeadTitle headTitle;
	private Context mContext;
	
	public HeadItemListener(Context context,HeadTitle headTitle) {
		this.headTitle = headTitle;
		mContext = context;
	}
	
	@Override
	public void onClick(View arg0) {
		if(headTitle.getType().equals("url")) {
			
		} else if(headTitle.getType().equals("magazine")) {
			Intent intent = new Intent(mContext, Article_Activity.class);
			intent.putExtra("MagazineID", Integer.valueOf(headTitle.getValue()));
			mContext.startActivity(intent);
			((Activity) mContext).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		} else {
			Intent intent = new Intent(mContext, Content_Activity.class);
			intent.putExtra("ArticleID", Integer.valueOf(headTitle.getValue()));
			mContext.startActivity(intent);
			((Activity) mContext).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left);
		}
		
	}

}
