package org.liurongchan.listener;

import org.liurongchan.young.Article_Activity;
import org.liurongchan.young.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * @author [FeN]July E-mail: newfenjuly@gmail.com
 * @version 创建时间：2013-11-8 下午9:29:27 类说明
 */
public class MagazineItemListener implements OnClickListener {

	private int magazineId;
	private Context mContext;

	public MagazineItemListener(Context context, int magazineId) {
		this.magazineId = magazineId;
		mContext = context;
	}

	@Override
	public void onClick(View v) {
		Intent intent = new Intent(mContext, Article_Activity.class);
		intent.putExtra("MagazineID", magazineId);
		mContext.startActivity(intent);
		((Activity) mContext).overridePendingTransition(R.anim.in_from_right, R.anim.out_to_left); 
	}

}